package org.mdev.revolution.communication.packets.outgoing;

public final class ServerPacketHeader {
    public static final short InitDiffieHandshakeComposer = 675;
    public static final short CompleteDiffieHandshakeComposer = 3179;
    public static final short UniqueMachineIDComposer = 2935;
    public static final short MOTDNotificationComposer = 1829;
    public static final short HabboBroadcastMessageComposer = 1279;
    public static final short AuthenticationOKComposer = 1442;
    public static final short AvailabilityStatusMessageComposer = 2468;
    public static final short NavigatorSettingsComposer = 3175;
    public static final short AvatarEffectsMessageComposer = 3310;
    public static final short FigureSetIdsComposer = 3469;
    public static final short UserRightsMessageComposer = 1862;
    public static final short AchievementsScoreComposer = 3710;
    public static final short AchievementsComposer = 2066;
    public static final short BuildersClubSubscriptionStatusMessageComposer = 2357;
    public static final short CfhChatlogComposer = 2067;
    public static final short AccountPreferencesComposer = 2921;
    public static final short UserObjectComposer = 1823;
    public static final short NoobnessLevelMessageComposer = 2807;
}