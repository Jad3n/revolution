package org.mdev.revolution.utilities;

import org.mdev.revolution.Revolution;

public class DefaultConfig {
    public static final String motd = "Revolution Server ~ Revolutionary Java Emulator targeting the Habbo flash client." +
            "\n\nYou can change this in your configuration file \"revolution.properties\"";
}
